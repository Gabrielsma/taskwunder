var currentTab = 0;
showTab(currentTab);

function showTab(n) {
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 2)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "New Payment";
    document.getElementById("prevBtn").style.display = 'none';
  }
  else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n)
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= (x.length - 1)) {
    if ($('#Submit').val() == 'Sending...') {
      return (false);
    }
    $('#Submit').val('Sending...');
    $.ajax({
      url: 'app/src/phpHandler.php',
      type: 'post',
      dataType: 'html',
      data: {
        'fName': $('#fName').val(),
        'lName': $('#lName').val(),
        'tel': $('#tel').val(),
        'street': $('#street').val(),
        'hNumber': $('#hNumber').val(),
        'zCode': $('#zCode').val(),
        'city': $('#city').val(),
        'aOwner': $('#aOwner').val(),
        'iban': $('#iban').val()
      }
    }).done(function (data) {
      var output = document.getElementById('output');
      output.innerHTML = data;
    });
  }else if(currentTab >= (x.length))
  {
    window.location.reload(true);
    return;
  }
  showTab(currentTab);
}

function validateForm() {
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}