<?php

namespace App;
require_once("models/Address.php");
use App\models\Address;
require_once("models/Customer.php");
use App\models\Customer;
require_once("models/Payment.php");
use App\models\Payment;

class CustomerModelView
{

    public function CreateCustomer($param)
    {
        $customer = new Customer($param['fName'], $param['lName'], $param['tel'], 
            new Address($param['street'], $param['hNumber'], $param['zCode'], $param['city']));
        $customer->saveCustomer();
        $customer->savePayment($param['aOwner'], $param['iban']);
        $result = $this->applyPayment($param['aOwner'], $param['iban'], $customer->getCustomerId());
        $customer->endPayment($result);
        echo $result;
    }

    private function applyPayment($aOwner, $iban, $customerId)
    {
        $postData = array(
            "customerId"    => $customerId,
            "iban"          => $iban,
            "owner"         => $aOwner
        );
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-Type: application/json; charset=utf-8",
                'content' => json_encode($postData)
            )
        ));
        $response = file_get_contents(
        'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', FALSE, $context);
        if($response === FALSE){
            die('Error');
        }
        $responseData = json_decode($response, TRUE);
        return $responseData['paymentDataId'];
    }    
}