<?php
namespace App\datalayer;

class MySqlConnection {

    var $host = "localhost";
    var $user = "root";
    var $senha = "";
    var $dbase = "task";

    var $link;

    public function connect(){
        $this->link = mysqli_connect($this->host,$this->user,$this->senha, $this->dbase);
        if (!$this->link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }

    public function noQuery($query){
        $this->connect();
        $this->link->query($query);
        $last_id = $this->link->insert_id;
        $this->link->close();
        return $last_id;
    }

    public function rdQuery($query){
        $this->connect();
        $result = $this->link->query($query);
        $this->link->close();
        return $result;
    }

    public function disconnect(){
        return mysql_close($this->link);
    }
}
