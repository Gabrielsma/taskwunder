<?php

namespace App\models;

require_once("dataLayer/MySqlConnection.php");
use App\dataLayer\MySqlConnection;

class Address{

    private $street;
    private $hNumber;
    private $zCode;
    private $city;
    private $addressId;
    private $customerId;

    public function __construct($street, $hNumber, 
    $zCode, $city)
    {
        $this->street = $street;      
        $this->hNumber = $hNumber;
        $this->zCode = $zCode;
        $this->city = $city;
    }

    public function saveAddress()
    {
        $mysql = new MySqlConnection();
        $sql = "INSERT INTO address (street, hNumber, zCode, city, customerId) " . 
        "VALUES('" . $this->street . "', '" . $this->hNumber . "', '" . $this->zCode . "', '" . $this->city . "', " . $this->customerId . ")";
        return $mysql->noQuery($sql);
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function getStreet()
    {
        return $this->street;
    }
    public function sethNumber($hNumber)
    {
        $this->hNumber = $hNumber;
    }

    public function gethNumber()
    {
        return $this->hNumber;
    }
    public function setzCode($zCode)
    {
        $this->zCode = $zCode;
    }

    public function getzCode()
    {
        return $this->zCode;
    }
    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }
}