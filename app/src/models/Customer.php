<?php

namespace App\models;

require_once("dataLayer/MySqlConnection.php");
use App\dataLayer\MySqlConnection;

class Customer 
{
    /**
     * Payment object
     * @var Payment
     */
    private $payment;
    /**
     * Address object
     * @var Address
     */
    private $address;

    private $firstName;
    private $lastName;
    private $telephone;
    private $customerId;

    public function __construct($firstName, $lastName, 
    $telephone, Address $address)
    {
        $this->firstName = $firstName;      
        $this->lastName = $lastName;
        $this->telephone = $telephone;
        $this->address = $address;
    }

    public function saveCustomer()
    {
        $mysql = new MySqlConnection();
        $sql = "INSERT INTO customer (firstname, lastname, telephone) " . 
        "VALUES('" . $this->firstName . "', '" . $this->lastName . "', '" . $this->telephone . "')";
        $this->customerId = $mysql->noQuery($sql);

        $this->address->setCustomerId($this->customerId);
        $this->address->saveAddress();
    }

    public function savePayment($aOwner, $iban)
    {
        $this->payment = new Payment($aOwner, $iban, $this->customerId);
        $this->payment->savePayment();
    }

    public function endPayment($paymentDataId)
    {
        $this->payment->endPayment($paymentDataId, $this->customerId);
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }
}