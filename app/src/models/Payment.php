<?php

namespace App\models;

require_once("dataLayer/MySqlConnection.php");
use App\dataLayer\MySqlConnection;
class Payment{

    private $iban;
    private $aOwner;
    private $paymentDataId;
    private $customerId;
    private $idpayment;

    public function __construct($aOwner, $iban, $customerId)
    {    
        $this->aOwner = $aOwner;
        $this->iban = $iban;
        $this->customerId = $customerId;
    }

    public function savePayment()
    {
        $mysql = new MySqlConnection();
        $sql = "INSERT INTO payment (aOwner, iban, customerId) " . 
        "VALUES('" . $this->aOwner . "','" . $this->iban . "', " . $this->customerId . ")";
        $this->idpayment = $mysql->noQuery($sql);
        return $this->idpayment;
    }

    public function endPayment($paymentDataId, $customerId)
    {
        $mysql = new MySqlConnection();
        $sql = "UPDATE payment SET paymentDataId = '$paymentDataId' WHERE customerId = $customerId";
        return $mysql->noQuery($sql);
    }

    public function setaOwner($aOwner)
    {
        $this->aOwner = $aOwner;
    }

    public function getaOwner()
    {
        return $this->aOwner;
    }
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    public function getIban()
    {
        return $this->iban;
    }
   
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }
    public function setIdPayment($idpayment)
    {
        $this->idpayment = $idpayment;
    }

    public function getIdPayment()
    {
        return $this->idpayment;
    }
}