<?php
namespace App;
require_once("CustomerModelView.php");

$data = array("fName" => $_POST['fName'], "lName" => $_POST['lName'], 
"tel" => $_POST['tel'], "street" => $_POST['street'], "hNumber" => $_POST['hNumber'],
"zCode" => $_POST['zCode'], "city" => $_POST['city'], "aOwner" => $_POST['aOwner'],
"iban" => $_POST['iban']);

$handler = new CustomerModelView();
return $handler->CreateCustomer($data);