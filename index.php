<?php
require 'vendor/autoload.php';

?>
<!DOCTYPE html>
<html>
  <head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" media="screen" href="/app/assets/wizard.css" />
<title>Task Wunder</title>
</head>
<body>
  <form id="regForm">
    <h1>Wunder Payment:</h1>
    <div class="tab">Personal Information:
      <p><input placeholder="First name: " oninput="this.className = ''" id="fName"></p>
      <p><input placeholder="Last name: " oninput="this.className = ''" id="lName"></p>
      <p><input placeholder="Telephone: " oninput="this.className = ''" id="tel"></p>
    </div>
    <div class="tab">Adress Information:
      <p><input placeholder="Street: " oninput="this.className = ''" id="street"></p>
      <p><input placeholder="House Number: " oninput="this.className = ''" id="hNumber"></p>
      <p><input placeholder="Zip Code: " oninput="this.className = ''" id="zCode"></p>
      <p><input placeholder="City: " oninput="this.className = ''" id="city"></p>
    </div>
    <div class="tab">Payment:
      <p><input placeholder="Account Owner" oninput="this.className = ''" id="aOwner"></p>
      <p><input placeholder="IBAN" oninput="this.className = ''" id="iban"></p>
    </div>
    <div class="tab"><h3>Sucess:</h3>
      <p>Your payment ID is : <div id="output"></div></p>
    </div>
    <div class="overflow">
      <div class="float-right">
        <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
        <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
      </div>
    </div>
    <div class="animation">
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>
    </div>
  </form>

  <script src="vendor/components/jquery/jquery.min.js"></script>
  <script src="/app/assets/wizard.js"></script>
</body>

</html>